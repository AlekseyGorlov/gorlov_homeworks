package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int[] elements = new int[201]; // -100..100

        while (a != -1) {
            elements[a+100]++;
            a = scanner.nextInt();
        }

        int minCount = 2147483647;
        int num = -99999999;

        // поиск числа, встречающегося наименьшее кол-во раз
        for (int i = 0; i < 201; i++) {
            if (minCount > elements[i] && elements[i] > 0) {
                minCount = elements[i];
                num = i - 100;
            }
        }

        System.out.println("Число, встречающееся наименьшее кол-во раз= "+ num);
    }
}
