package ru.pcs;

import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
    List<Product> findAllByPrice(double price);
    List<Product> findAllByOrdersCount(int ordersCount);
}
