package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_FIND_ALL = "select * from product order by id";
    //language=SQL
    private static final String SQL_FIND_ALL_BY_PRICE = "select * from product where price = ?";
    //language=SQL
    private static final String SQL_FIND_ALL_BY_ORDERS_COUNT = "select * from product p where (select count(1) from \"order\" o where o.product_id = p.id) = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapperRowMapper = (row, rowNumber) -> {
        Product product = new Product();

        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int amount = row.getInt("amount");

        return new Product(id, description, price, amount);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, productRowMapperRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_PRICE, productRowMapperRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_FIND_ALL_BY_ORDERS_COUNT, productRowMapperRowMapper, ordersCount);
    }
}
