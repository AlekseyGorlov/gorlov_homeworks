package ru.pcs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;

public class Main {

    public static final double PRICE = 5.25;
    public static final int ORDERS_COUNT = 2;

    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2", "postgres", "postgres");

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        System.out.println("Все товары:");
        System.out.println(productsRepository.findAll());
        System.out.println("Товары с ценой "+ PRICE + ": ");
        System.out.println(productsRepository.findAllByPrice(PRICE));
        System.out.println("Товары по количеству заказов = "+ ORDERS_COUNT +", в которых участвуют:");
        System.out.println(productsRepository.findAllByOrdersCount(ORDERS_COUNT));
    }
}
