import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int countMin = 0;
        int prevPrevA = -1;
        int prevA = -1;

        int a = scanner.nextInt();

		while (a != -1) {
			if (prevA != -1) {
				if (prevPrevA == -1) {
					if (a > prevA) countMin++;
				}
				else {
					if (prevPrevA > prevA && prevA < a) countMin++;
				}	
			}
			prevPrevA = prevA;
			prevA = a;
			a = scanner.nextInt();	
		}

		if (prevPrevA > prevA) countMin++;

		System.out.println("Number of local mins= "+ countMin);
	}
	
}