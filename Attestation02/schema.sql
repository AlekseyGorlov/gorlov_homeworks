-- создание и заполнение таблицы товаров
create table product (
                         id serial primary key,
                         description varchar(100),
                         price decimal(10,2),
                         amount integer
);

insert into product (description, price, amount)  values('цветные карандаши', 100.50, 25);
insert into product (description, price, amount)  values('фломастеры', 99.00, 30);
insert into product (description, price, amount)  values('тетрадь', 5.25, 78);
insert into product (description, price, amount)  values('ручка', 15.99, 40);
insert into product (description, price, amount)  values('альбом', 50.50, 125);
insert into product (description, price, amount)  values('линейка', 2.50, 250);

-- создание и заполнение таблицы покупателей
create table customer (
                          id serial primary key,
                          first_name varchar(30),
                          last_name varchar(30)
);

insert into customer (first_name, last_name) values ('Марсель', 'Сидиков');
insert into customer (first_name, last_name) values ('Виктор', 'Викторов');
insert into customer (first_name, last_name) values ('Иван', 'Иванов');
insert into customer (first_name, last_name) values ('Максим', 'Максимов');
insert into customer (first_name, last_name) values ('Антон', 'Антонов');
insert into customer (first_name, last_name) values ('Алексей', 'Алексеев');

-- создание и заполнение таблицы заказов
create table "order" (
                         product_id integer, foreign key(product_id) references product(id),
                         customer_id integer, foreign key(customer_id) references customer(id),
                         order_date date,
                         order_amount integer check (order_amount > 0 and order_amount < 1000000)
);

insert into "order" (product_id, customer_id, order_date, order_amount) values (1, 1, '1.02.2021', 5);
insert into "order" (product_id, customer_id, order_date, order_amount) values (1, 1, '21.02.2021', 1);
insert into "order" (product_id, customer_id, order_date, order_amount) values (1, 1, '5.02.2021', 10);
insert into "order" (product_id, customer_id, order_date, order_amount) values (5, 2, '2.02.2021', 20);
insert into "order" (product_id, customer_id, order_date, order_amount) values (6, 2, '3.02.2021', 30);
insert into "order" (product_id, customer_id, order_date, order_amount) values (2, 3, '25.02.2021', 7);
insert into "order" (product_id, customer_id, order_date, order_amount) values (2, 3, '5.02.2021', 9);
insert into "order" (product_id, customer_id, order_date, order_amount) values (3, 4, '5.02.2021', 11);
insert into "order" (product_id, customer_id, order_date, order_amount) values (4, 4, '1.02.2021', 22);
insert into "order" (product_id, customer_id, order_date, order_amount) values (4, 5, '22.02.2021', 15);

-- покупатель/товар/сколько шт в сумме заказал
select     c.first_name||' '||c.last_name, p.description, sum(o.order_amount)
from       customer c
               inner join "order" o on o.customer_id = c.id
               inner join product p on p.id = o.product_id
group by   c.first_name||' '||c.last_name, p.description;

-- сколько осталось товаров на складе после заказов
select p.description, sum(t.amount)
from (
         select p.id, p.amount
         from   product p
         union all
         select o.product_id id, -o.order_amount amount
         from   "order" o
     ) t
         inner join product p on p.id = t.id
group by   p.description;

-- кто сделал наибольшее кол-во заказов
select distinct
       first_value(c.first_name||' '||c.last_name) over (order by t.cnt desc)
from (
         select o.customer_id, count(1) cnt
         from "order" o
         group by o.customer_id
     ) t
         inner join customer c on c.id = t.customer_id;

-- календарь заказов (сколько заказов в каждый день февраля)
select t.order_date, coalesce(sum(o.order_amount), 0)
from
    (
        select day::date order_date
        from generate_series('2021-02-01', '2021-02-28', interval '1 day') day
    ) t
        left join "order" o on o.order_date = t.order_date
group by   t.order_date;

-- Заказы по дням (сколько заказов в какой день февраля с разбивкой по покупателям с их заказами)
select t.order_date, string_agg(c.first_name||' '||c.last_name||' ['||p.description||' '||o.order_amount||']', ', ')
from
    (
        select day::date order_date
        from generate_series('2021-02-01', '2021-02-28', interval '1 day') day
    ) t
        inner join "order" o on o.order_date = t.order_date
        inner join customer c on c.id = o.customer_id
        inner join product p on p.id = o.product_id
group by t.order_date;
