package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] array;

    public static void main(String[] args) {
        Random random = new Random();
	    Scanner scanner = new Scanner(System.in);

        System.out.println("Размерность массива: ");
        int numbersCount = scanner.nextInt();
        System.out.println("Количество потоков: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;
        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }
        System.out.println("Сумма из потока main= "+ realSum);

        SumThread[] sumThread = new SumThread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            sumThread[i] = new SumThread( i * numbersCount / threadsCount + ((i == 0) ? 0 : 1)
                                        , (i + 1) * numbersCount / threadsCount + ((i == threadsCount - 1) ? -1 : 0));
            sumThread[i].start();
            try {
                sumThread[i].join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }

        int byThreadsSum = 0;
        for (int i = 0; i < threadsCount; i++) {
            byThreadsSum += sumThread[i].getSum();;
        }
        System.out.println("Сумма из потоков= "+ byThreadsSum);
    }
}
