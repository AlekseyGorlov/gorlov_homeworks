package com.company;

import static com.company.Main.array;

public class SumThread extends Thread {
    private int from;
    private int to;
    private int sum;

    @Override
    public void run() {
        for (int i = getFrom(); i <= getTo(); i++) {
            this.sum += array[i];
        }
    }

    public SumThread(int from, int to) {
        setFrom(from);
        setTo(to);
        setSum(0);
    }

    public int getSum() { return sum; }

    public void setSum(int sum) { this.sum = sum; }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
