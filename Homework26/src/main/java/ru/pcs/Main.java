package ru.pcs;

public class Main {
    public static void main(String[] args) {
        NumberUtil numberUtil = new NumberUtil();

        System.out.println(numberUtil.isPrime(13));
        System.out.println(numberUtil.isPrime(31));
        System.out.println(numberUtil.isPrime(169));
        System.out.println(numberUtil.isPrime(172));
        System.out.println(numberUtil.isPrime(2));
        System.out.println(numberUtil.isPrime(3));

        System.out.println(numberUtil.gcd(18, 12));
        System.out.println(numberUtil.gcd(9, 12));
        System.out.println(numberUtil.gcd(64, 48));
    }
}
