package ru.pcs;

public class NumberUtil {
    // является ли число простым
    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    public int sum(int a, int b) {
        return a + b;
    }

    /*
    нод(18, 12) = 6
    нод(9, 12) = 3
    нод(64, 48) = 16
     */
    public int gcd(int a, int b) {

        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException();
        }

        int aa = a, bb = b;

        while (aa != bb) {
            if (aa > bb) {
                aa = aa - bb;
            } else {
                bb = bb - aa;
            }
        }
        return aa;
    }
}
