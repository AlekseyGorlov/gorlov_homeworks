package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumberUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumberUtilTest {

    private final NumberUtil numberUtil = new NumberUtil();

    @Nested
    @DisplayName("isPrime is working")
    public class ForIsPrime {
        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 71, 113})
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {22, 33, 72, 114})
        public void on_not_prime_numbers_return_false(int number) {
            assertFalse(numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sqr_numbers_return_false(int number) {
            assertFalse(numberUtil.isPrime(number));
        }

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, 1})
        public void bad_numbers_throws_exceptions(int number) {
            assertThrows(IllegalArgumentException.class, () -> numberUtil.isPrime(number));
        }
    }

    @Nested
    @DisplayName("sum() is working")
    public class ForSum {
        @ParameterizedTest(name = "return {2} on {0} + {1}")
        @CsvSource(value = {"5, 10, 15", "4, 2, 6", "11, -2, 9"})
        public void return_correct_sum(int a, int b, int result) {
            assertEquals(result, numberUtil.sum(a, b));
        }
    }

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {
        @ParameterizedTest(name = "return {2} on {0} + {1}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "64, 48, 16"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numberUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on {0}, {1}")
        @CsvSource(value = {"9, -3"})
        public void bad_numbers_throws_exceptions(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numberUtil.gcd(a, b));
        }
     }

}