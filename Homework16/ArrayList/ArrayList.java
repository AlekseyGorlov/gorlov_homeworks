package ArrayList;

import java.util.Arrays;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    /**
     * Добавляет элемент в конец списка
     * @param element - добавляемый элемент
     */
    public void add(T element) {
         if (isFullArray()) {
             // запоминаем старый массив
             T[] oldElements = this.elements;
             // создаем новый массив, который в полтора раза больше предыдущего
             this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
             // копируем все элементы из старого массива в новый
             for (int i=0; i < size; i++) {
                 this.elements[i] = oldElements[i];
             }
        }

        this.elements[size] = element;
        size++;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Получить элемент по индексу
     * @param index - индекс элемента
     * @return
     */
    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            return null;
        }
    }

    /**
     * Удаление элемента по индексу
     * @param index - индекс удаляемого элемента
     */
    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            // запоминаем старый массив
            T[] oldElements = this.elements;
            // создаем новый массив, который на один элемент меньше
            this.elements = (T[]) new Object[size - 1];

            int j = 0;
            for(int i = 0; i < size; i++) {
                if (index != i) {
                    this.elements[j] = oldElements[i];
                    j++;
                }
            }

            size--;
        }
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.print(this.elements[i] + " ");
        }
        System.out.println();
    }
}
