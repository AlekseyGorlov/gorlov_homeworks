package ArrayList;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        ArrayList<Integer> numbers = new ArrayList();
        numbers.add(33);
        numbers.add(11);
        numbers.add(3);
        numbers.add(-34);
        numbers.add(67);
        numbers.add(92);
        numbers.add(12);
        numbers.add(10);
        numbers.add(99);
        numbers.add(333);
        numbers.add(567);
        numbers.add(987);
        numbers.add(-257);
        numbers.add(33);
        numbers.add(-1);
        numbers.add(126);
        numbers.add(90);

        numbers.print();

        numbers.removeAt(3);

        numbers.print();
    }

}
