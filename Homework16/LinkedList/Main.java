package LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();

        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);

        list.print();

        list.addToBegin(66);
        list.addToBegin(77);
        list.addToBegin(88);

        list.print();

        System.out.println("3й элемент= " + list.get(3));

        list.addAt(-1, 1);

        list.print();
    }
}
