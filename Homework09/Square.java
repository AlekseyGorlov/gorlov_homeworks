package com.company;

public class Square extends Rectangle {

    public Square(int x) {
        super(x, x);
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public String toString() {
        return String.format("Периметр квадрата= %.2f", this.getPerimeter());
    }
}
