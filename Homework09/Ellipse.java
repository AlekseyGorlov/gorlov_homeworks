package com.company;

public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter() {
        return 4 * (Math.PI*x*y + x - y) / (x + y);
    }

    @Override
    public String toString() {
        return String.format("Периметр эллипса= %.2f", this.getPerimeter());
    }
}
