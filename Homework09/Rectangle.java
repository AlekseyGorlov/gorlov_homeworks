package com.company;

public class Rectangle extends Figure {

    public Rectangle(int x, int y) {
       super(x, y);
    }

    @Override
    public double getPerimeter() {
        return 2*(x + y);
    }

    @Override
    public String toString() {
        return String.format("Периметр прямоугольника= %.2f", this.getPerimeter());
    }
}
