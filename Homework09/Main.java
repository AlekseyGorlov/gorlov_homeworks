package com.company;

public class Main {

    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(1, 2);
	    Circle circle = new Circle(1);
        Rectangle rectangle = new Rectangle(2,3);
        Square square = new Square(4);

        Figure[] figure = new Figure[4];
        figure[0] = ellipse;
        figure[1] = circle;
        figure[2] = rectangle;
        figure[3] = square;

        for (int i = 0; i < figure.length; i++) {
            System.out.println(figure[i].toString());
        }
    }
}
