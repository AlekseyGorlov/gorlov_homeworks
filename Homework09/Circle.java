package com.company;

public class Circle extends Ellipse {

    public Circle(int x) {
        super(x, x);
    }

    @Override
    public double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public String toString() {
        return String.format("Периметр окружности= %.2f", this.getPerimeter());
    }
}
