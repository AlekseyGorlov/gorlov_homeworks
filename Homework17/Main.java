package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        Scanner scanner = new Scanner(System.in);
        String[] words = scanner.nextLine().split(" ");

        for (int i = 0; i < words.length; i++)
            map.put(words[i], (map.get(words[i]) == null) ? 1 : map.get(words[i]) + 1);

        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        for(Map.Entry<String, Integer> entry : entries) {
            System.out.println(entry.getKey() +" - "+ entry.getValue());
        }
    }
}
