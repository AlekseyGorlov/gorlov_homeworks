package com.company;

public class Human {
    private String name;
    private double weight;

    public String getName() {
        return this.name;
    }

    public double getWeight() {
        return this.weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight > 0 && weight < 500)
            this.weight = weight;
    }

    @Override
    public String toString() {
        return String.format(name +" %.2f ", weight);
    }
}
