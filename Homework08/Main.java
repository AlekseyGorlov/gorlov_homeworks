package com.company;

public class Main {

    public static void main(String[] args) {
	    Human[] humans = new Human[10];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setName("User" + i);
            humans[i].setWeight(Math.random()*100 + 20);
        }

        for (int i = 0; i < humans.length; i++) {
            System.out.printf(humans[i].getName() +" %.2f ", humans[i].getWeight());
        }

        // сортировка объектов методом пузырька по полю weight
        boolean sorted = false;
        Human temp;
        while(!sorted) {
            sorted = true;
            for (int i = 0; i < humans.length - 1; i++) {
                if (humans[i].getWeight() > humans[i+1].getWeight()) {
                    temp = humans[i];
                    humans[i] = humans[i+1];
                    humans[i+1] = temp;
                    sorted = false;
                }
            }
        }

        System.out.println();
 
        for (int i = 0; i < humans.length; i++) {
            System.out.print(humans[i].toString());
        }

    }
}
