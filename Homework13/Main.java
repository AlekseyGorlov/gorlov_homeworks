package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {-44, 0, 11, 2, 3, 33, 5, 6, 88};
        System.out.println("Исходный массив: " + Arrays.toString(array));

        int[] a = Sequence.filter(array, number -> number % 2 == 0);
        System.out.println("Четные элементы: " + Arrays.toString(a));

        int[] b = Sequence.filter(array, new ByCondition() {
            @Override
            public boolean isOk(int number) {
                int result = 0;
                number = Math.abs(number);
                while (number > 0) {
                    result = result + number % 10;
                    number /= 10;
                }
                return result % 2 == 0;
            }
        });
        System.out.println("Сумма цифр - четная: " + Arrays.toString(b));
    }
}
