package com.company;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] array2 = new int[array.length];
        int size = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                array2[i] = array[i];
                size++;
            } else{
                array2[i] = -1;
            }
        }

        int[] result = new int[size];
        size = 0;
        for (int i = 0; i < array.length; i++) {
            if (array2[i] != -1) {
                result[size] = array2[i];
                size++;
            }
        }
        return result;
    }
}
