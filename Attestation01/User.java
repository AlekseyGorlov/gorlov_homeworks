package com.company;

public class User {
    private int id;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int id, String name, int age, boolean isWorker) {
        setId(id);
        setName(name);
        setAge(age);
        setWorker(isWorker);
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public boolean getIsWorker() {
        return isWorker;
    }
    public void setWorker(boolean worker) {
        isWorker = worker;
    }
}
