package com.company;

public class Main {

    public static void main(String[] args) {
	    UsersRepository users = new UsersRepository("users.txt");

        User user;
        try {
            user = users.findById(1);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return;
        }
        user.setName("Марсель");
        user.setAge(27);
        users.update(user);
    }
 }
