package com.company;

import java.io.*;

public class UsersRepository{
    private String fileName;
    private String fileNameTmp;

    public UsersRepository(String fileName) {
        setFileName(fileName);
        setFileNameTmp(fileName +"$$$");
    }

    public String getFileName() { return fileName; }
    public void setFileName(String fileName) { this.fileName = fileName; }

    public String getFileNameTmp() { return fileNameTmp; }
    public void setFileNameTmp(String fileNameTmp) { this.fileNameTmp = fileNameTmp; }

    public User findById(int id) throws Exception {
        try (BufferedReader reader = new BufferedReader(new FileReader(getFileName()))) {
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                if (Integer.valueOf(parts[0]) == id) {
                    User user = new User(Integer.valueOf(parts[0]), parts[1], Integer.valueOf(parts[2]), Boolean.valueOf(parts[3]));
                    return user;
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        throw new Exception("Не существует пользователя с таким id");
    }

    void update(User user) {
        try {
            new File(getFileName()).renameTo(new File(getFileNameTmp()));
            BufferedReader reader = new BufferedReader(new FileReader(getFileNameTmp()));
            BufferedWriter writer = new BufferedWriter(new FileWriter(getFileName()));

            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");

                if (Integer.valueOf(parts[0]) == user.getId()) {
                    line = String.format("%d|%s|%d|%s", user.getId(), user.getName(), user.getAge(), user.getIsWorker());
                }
                writer.append(line +"\n");

                line = reader.readLine();
            }

            writer.flush();
            writer.close();
            reader.close();

            new File(getFileNameTmp()).delete();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}