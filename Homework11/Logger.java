package com.company;

public class Logger {

    public static final Logger instance;

    static {
        instance = new Logger();
    }

    public static Logger getInstance() {
        return instance;
    }

    public void log(String message) {
        System.out.println(message);
    }
}
