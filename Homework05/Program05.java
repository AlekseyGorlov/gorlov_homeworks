import java.util.Scanner;

class Program05 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int minDigit = 9;
		int curDigit;
	    int a = scanner.nextInt();

		while (a != -1) {
			while (a > 0) {
				curDigit = a % 10;
				if (minDigit > curDigit)
					minDigit = curDigit; 
				a = a / 10;
			}	
	
			a = scanner.nextInt();	
		}

		System.out.println("Minimal digit= "+ minDigit);
	}
	
}