package com.company;

public class Car implements Comparable<Car> {
    String number;
    String model;
    String color;
    int mileage;
    int price;

    public Car(String number, String model, String color, int mileage, int price) {
        setNumber(number);
        setModel(model);
        setColor(color);
        setMileage(mileage);
        setPrice(price);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public int getPrice() { return price; }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int compareTo(Car arg) {
        return this.getPrice() - arg.getPrice();
    }
}
