package com.company;

public abstract class Figure {

    protected int x;
    protected int y;

    public abstract int getX();
    public abstract int getY();

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
