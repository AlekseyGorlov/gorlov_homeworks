package com.company;

public class Ellipse extends Figure {

    protected double a;
    protected double b;

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Ellipse(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }
}
