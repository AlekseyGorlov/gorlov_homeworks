package com.company;

public interface Movement {

    public void move(int x, int y);

}
