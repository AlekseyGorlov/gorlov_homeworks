package com.company;

public class Main {

    public static void main(String[] args) {

        Logger logger1 = Logger.getInstance();
        Logger logger2 = Logger.getInstance();

        logger1.log("Открытие файла на чтение");
        logger1.log("Открытие файла на запись");
        logger2.log("Запись в файл");
        logger2.log("Чтение из файла");
    }
}
