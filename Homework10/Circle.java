package com.company;

public class Circle extends Ellipse implements Movement {

    public Circle(int x, int y, double a) {
        super(x, y, a, a);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}