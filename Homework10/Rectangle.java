package com.company;

public class Rectangle extends Figure {

    protected double a;
    protected double b;

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Rectangle(int x, int y, double a, double b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }
}
