package com.company;

public class Square extends Rectangle implements Movement {

    public Square(int x, int y, double a) {
        super(x, y, a, a);
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}