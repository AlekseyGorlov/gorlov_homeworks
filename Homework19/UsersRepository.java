package com.company;

import java.util.List;

public interface UsersRepository {
    List<User> findByCondition(ByCondition condition);
    void save(User user);
}
