package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findByCondition(ByCondition condition) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");

                String name = parts[0];
                int age = Integer.parseInt(parts[1]);
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                if (condition.isOk(age, isWorker)) {
                    User newUser = new User(name, age, isWorker);
                    users.add(newUser);
                }

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException ignore) {}

            if (bufferedReader != null)
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {}
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(this.fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.newLine();
            bufferedWriter.write(user.getName() +"|"+ user.getAge() +"|"+ user.isWorker());
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
        }
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException ignore) {}
        }
    }
}
