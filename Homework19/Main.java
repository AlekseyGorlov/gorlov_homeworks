package com.company;

import java.util.List;

public class Main {
    public static final int AGE_VALUE = 22;
    public static final boolean IS_WORKER_VALUE = true;

    public static void main(String[] args) {
	    UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

        List<User> users = usersRepository.findByCondition((age, isWorker) -> true);
        for(User user : users) {
            System.out.println(user.getName() +" "+ user.getAge() +" "+ user.isWorker());
        }

        User user = new User("Игорь", 33, false);
        usersRepository.save(user);


        System.out.println();
        System.out.println("Пользователи возраста "+ AGE_VALUE +":");
        List<User> users2 = usersRepository.findByCondition((age, isWorker) -> (age == AGE_VALUE));
        for(User user2 : users2) {
            System.out.println(user2.getName() +" "+ user2.getAge() +" "+ user2.isWorker());
        }

        System.out.println();
        System.out.println("Пользователи работающие:");
        List<User> users3 = usersRepository.findByCondition((age, isWorker) -> (isWorker == IS_WORKER_VALUE));
        for(User user3 : users3) {
            System.out.println(user3.getName() +" "+ user3.getAge() +" "+ user3.isWorker());
        }
    }
}
