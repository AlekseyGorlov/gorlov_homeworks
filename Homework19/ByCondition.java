package com.company;

public interface ByCondition {
    boolean isOk(int age, boolean isWorker);
}
