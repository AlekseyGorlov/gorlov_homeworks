package com.company;
import java.util.Scanner;

public class Main {

    /**
     *
     * @param array массив целых чисел
     * @param number целое число
     * @return индекс числа в массиве (-1 если не найдено)
     */

    public static int getNumIndex(int[] array, int number) {

        for (int i=0; i < array.length; i++) {
          if (array[i] == number)
              return i;
        }
        return -1;
    }

    /**
     * Перемещает значимые элементы массива влево
     * @param array массив целых чисел
     */

    public static void moveElements(int[] array) {

        int zeroIndex;

        for (int i = 0; i < array.length; i++) {

            zeroIndex = 0;
            while (array[zeroIndex] != 0)
                zeroIndex++;

            if (i > zeroIndex) {
                array[zeroIndex] = array[i];
                array[i] = 0;
            }
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int[] array = {9,4,1,6,3,0,8,5,2,4};
        int[] array2 = {39,0,0,16,33,0,8,-1,0,0,6};

        System.out.println("Индекс числа "+ a +" в массиве= "+ getNumIndex(array, a));

        for (int i = 0; i < array2.length; i++)
            System.out.print(array2[i] + " ");

        moveElements(array2);

        System.out.println();
        for (int i = 0; i < array2.length; i++)
            System.out.print(array2[i] + " ");
        System.out.println();

    }
}
